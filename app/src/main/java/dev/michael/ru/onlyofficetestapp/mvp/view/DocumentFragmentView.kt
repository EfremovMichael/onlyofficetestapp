package dev.michael.ru.onlyofficetestapp.mvp.view

import dev.michael.ru.onlyofficetestapp.model.document.MyFiles

interface DocumentFragmentView: View {

    fun showMessage(message: String?)
    fun updateUi(myFiles: MyFiles)

}