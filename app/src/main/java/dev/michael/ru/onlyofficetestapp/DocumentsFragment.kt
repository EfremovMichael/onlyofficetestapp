package dev.michael.ru.onlyofficetestapp

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import dev.michael.ru.onlyofficetestapp.adapter.FilesAdapter
import dev.michael.ru.onlyofficetestapp.adapter.OnFileClickListener
import dev.michael.ru.onlyofficetestapp.di.App
import dev.michael.ru.onlyofficetestapp.model.document.File
import dev.michael.ru.onlyofficetestapp.model.document.Folder
import dev.michael.ru.onlyofficetestapp.model.document.MyFiles
import dev.michael.ru.onlyofficetestapp.mvp.presenter.DocumentFragmentPresenter
import dev.michael.ru.onlyofficetestapp.mvp.view.DocumentFragmentView
import kotlinx.android.synthetic.main.app_bar_main.*

//Main fragment

class DocumentsFragment : Fragment(), DocumentFragmentView, OnFileClickListener {

    private lateinit var presenter: DocumentFragmentPresenter
    private lateinit var adapter: FilesAdapter

    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_documents, container, false)

        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(activity)

        presenter.attachView(this)

        when {
            arguments?.getString(TITLE) != null -> {
                presenter.loadFolder(arguments!!.getInt(DocumentsFragment.ID_FOLDER))
                activity?.toolbar?.title = arguments?.getString(TITLE).toString()
            }
            arguments?.getInt(KEY_FOLDER) == 0 -> {
                activity?.supportFragmentManager?.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                presenter.loadMyFiles()
            }
            arguments?.getInt(KEY_FOLDER) == 1 -> {
                activity?.supportFragmentManager?.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                presenter.loadCommonFiles()
            }
        }
        return view
    }

    override fun onAttach(context: Context?) {
        presenter = App.getComponent().getDocumentFragmentPresenter()
        super.onAttach(context)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun showMessage(message: String?) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    override fun updateUi(myFiles: MyFiles) {
        adapter = FilesAdapter(myFiles, this)
        recyclerView.adapter = adapter
    }

    override fun folderClick(idFolder: Int, title: String) {
        activity!!.supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragmentContainer, newInstanceByFolder(idFolder, title))
                .addToBackStack("back").commit()
    }

    override fun fileClick(url: String) {
        showMessage(url)
    }

    override fun infoClick(folder: Folder) {
        showInfo(folder)
    }

    override fun infoClick(file: File) {
        showInfo(file)
    }

    private fun showInfo(file: File) {
        val alertDialog = AlertDialog.Builder(context!!)
        alertDialog.setTitle(file.title)
        alertDialog.setMessage("Type: ${file.fileType} \n ")
        alertDialog.setPositiveButton("Ok") { _, _ -> }
        alertDialog.show()
    }

    private fun showInfo(folder: Folder) {
        val alertDialog = AlertDialog.Builder(context!!)
        alertDialog.setTitle(folder.title)
        alertDialog.setMessage("Files count: ${folder.filesCount} \n ")
        alertDialog.setPositiveButton("Ok") { _, _ -> }
        alertDialog.show()
    }

    companion object {

        const val KEY_FOLDER = "KEY_FOLDER"
        const val ID_FOLDER = "ID_FOLDER"
        const val TITLE = "TITLE"
        const val FILE = "FILE"

        @JvmStatic
        fun newInstance(keyFolder: Int): DocumentsFragment {
            val arguments = Bundle()
            val documentsFragment = DocumentsFragment()
            arguments.putInt(KEY_FOLDER, keyFolder)
            documentsFragment.arguments = arguments
            return documentsFragment
        }

        @JvmStatic
        fun newInstanceByFolder(idFolder: Int, title: String): DocumentsFragment {
            val argumentsByFolder = Bundle()
            val documentsFragment = DocumentsFragment()
            argumentsByFolder.apply {
                putInt(ID_FOLDER, idFolder)
                putString(TITLE, title)
            }
            documentsFragment.arguments = argumentsByFolder
            return documentsFragment
        }

    }
}
