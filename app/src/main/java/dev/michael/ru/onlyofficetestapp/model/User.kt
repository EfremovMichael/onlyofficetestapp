package dev.michael.ru.onlyofficetestapp.model

data class User(
        val status: Int,
        val response: Response
)

data class Response(
        val id: String,
        val userName: String,
        val isVisitor: Boolean,
        val firstName: String,
        val lastName: String,
        val email: String,
        val birthday: String,
        val sex: String,
        val status: Int,
        val activationStatus: Int,
        val terminated: String,
        val department: String,
        val workFrom: String,
        val location: String,
        val notes: String,
        val displayName: Any,
        val title: String,
        val contacts: List<Contact>,
        val groups: List<Group>,
        val avatarMedium: String,
        val avatar: String,
        val isOnline: Boolean,
        val isAdmin: Boolean,
        val isLDAP: Boolean,
        val listAdminModules: List<String>,
        val isOwner: Boolean,
        val cultureName: String,
        val isSSO: Boolean,
        val avatarSmall: String,
        val profileUrl: String
)

data class Contact(
        val type: String,
        val value: String
)

data class Group(
        val id: String,
        val name: String,
        val manager: String
)