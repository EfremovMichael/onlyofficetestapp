package dev.michael.ru.onlyofficetestapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import dev.michael.ru.onlyofficetestapp.R
import dev.michael.ru.onlyofficetestapp.model.document.DocType
import dev.michael.ru.onlyofficetestapp.model.document.File
import dev.michael.ru.onlyofficetestapp.model.document.Folder
import dev.michael.ru.onlyofficetestapp.model.document.MyFiles
import dev.michael.ru.onlyofficetestapp.utils.inflate

interface OnFileClickListener {

    fun folderClick(idFolder: Int, title: String)
    fun fileClick(url: String)
    fun infoClick(folder: Folder)
    fun infoClick(file: File)
}

class FilesAdapter(private val myFiles: MyFiles,
                   private val onFileClickListener: OnFileClickListener) : RecyclerView.Adapter<FilesAdapter.FilesHolder>() {

    class FilesHolder(private val v: View) : RecyclerView.ViewHolder(v) {

        private val fileImageView = v.findViewById<ImageView>(R.id.fileImageView)
        private val fileNameTv = v.findViewById<TextView>(R.id.fileNameTv)
        private val fileDateTv = v.findViewById<TextView>(R.id.fileDateTv)
        private val fileInfoImageView = v.findViewById<ImageView>(R.id.fileInfoImageView)

        fun bindFolder(folder: Folder, listener: OnFileClickListener) {
            folder.apply {
                fileNameTv.text = title
                fileDateTv.text = parseDate(updated)
                fileImageView.setImageResource(R.drawable.ic_folder)
                v.setOnClickListener { listener.folderClick(folder.id, folder.title) }
                fileInfoImageView.setOnClickListener { listener.infoClick(folder) }
            }
        }

        private fun parseDate(date: String): CharSequence? {
            val builder = StringBuilder()
            for (i in 0..9) {
                builder.append(date[i])
            }
            return builder.toString()
        }

        fun bindFile(file: File, listener: OnFileClickListener) {
            file.apply {
                fileNameTv.text = title
                fileDateTv.text = parseDate(updated)
                fileImageView.setImageResource(setImage(fileType))
                v.setOnClickListener { listener.fileClick(webUrl) }
                fileInfoImageView.setOnClickListener { listener.infoClick(file) }
            }
        }

        private fun setImage(fileType: Int): Int =
                when (fileType) {
                    DocType.WORD.type -> R.drawable.ic_word
                    DocType.EXEL.type -> R.drawable.ic_exel
                    DocType.POWERPOINT.type -> R.drawable.ic_powerpoint
                    else -> R.drawable.ic_launcher_foreground
                }
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): FilesAdapter.FilesHolder {
        val view = parent.inflate(R.layout.file_item, false)
        return FilesHolder(view)
    }

    override fun getItemCount(): Int = myFiles.run { folders.size + files.size }

    override fun onBindViewHolder(holder: FilesAdapter.FilesHolder, position: Int) {
        if (position < myFiles.folders.size) {
            holder.bindFolder(myFiles.folders[position], onFileClickListener)
        } else {
            holder.bindFile(myFiles.files[position - myFiles.folders.size], onFileClickListener)
        }

    }
}