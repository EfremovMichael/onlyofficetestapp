package dev.michael.ru.onlyofficetestapp.mvp.presenter

import dev.michael.ru.onlyofficetestapp.mvp.view.View

interface Presenter<T : View> {

    fun attachView(view: T)

    fun detachView()

}