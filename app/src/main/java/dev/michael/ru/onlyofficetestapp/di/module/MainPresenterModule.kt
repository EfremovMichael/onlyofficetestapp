package dev.michael.ru.onlyofficetestapp.di.module

import dagger.Module
import dagger.Provides
import dev.michael.ru.onlyofficetestapp.mvp.presenter.MainPresenterImpl
import dev.michael.ru.onlyofficetestapp.network.OnlyOfficeImpl
import dev.michael.ru.onlyofficetestapp.storage.SharedPreferences
import javax.inject.Singleton

@Module(includes = [NetworkModule::class, StorageModule::class])
class MainPresenterModule {

    @Singleton
    @Provides
    fun provideMainPresenter(preferences: SharedPreferences, onlyOfficeImpl: OnlyOfficeImpl) =
            MainPresenterImpl(preferences, onlyOfficeImpl)
}