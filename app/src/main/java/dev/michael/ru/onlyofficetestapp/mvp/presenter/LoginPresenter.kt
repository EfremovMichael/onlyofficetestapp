package dev.michael.ru.onlyofficetestapp.mvp.presenter

import android.util.Log
import dev.michael.ru.onlyofficetestapp.mvp.view.LoginView
import dev.michael.ru.onlyofficetestapp.network.OnlyOffice
import dev.michael.ru.onlyofficetestapp.storage.SharedPreferences
import io.reactivex.disposables.CompositeDisposable
import java.util.regex.Pattern

// Login presenter by login view
interface LoginPresenter : Presenter<LoginView> {

    fun getToken(login: String, password: String)
    fun checkToken()
    fun deleteToken()

}

class LoginPresenterImpl(
        private val onlyOffice: OnlyOffice,
        private val sharedPreferences: SharedPreferences) : LoginPresenter {

    private var view: LoginView? = null
    private val disposable = CompositeDisposable()

    override fun attachView(view: LoginView) {
        this.view = view
    }

    override fun detachView() {

        disposable.clear()
        view = null
    }

    override fun getToken(login: String, password: String) {
        view?.showProgressBar()
        if (validateAuth(login, password)) {
            disposable.add(onlyOffice.getToken(login, password)
                    .subscribe { auth, error ->
                        if (error == null) {
                            writeToken(auth.response.token)
                            view?.goNextActivity()
                        } else {
                            view?.showMessage(error.message.toString())
                        }
                    })
        }
        view?.hideProgressBar()
    }

    override fun checkToken() {
        view?.showProgressBar()
        val token = sharedPreferences.read()
        Log.d("myLog", token.toString())
        if (token != null) {
            view?.goNextActivity()
        }
        view?.hideProgressBar()
    }

    override fun deleteToken() {
        Log.d("myLog", "delete")
        sharedPreferences.delete()
    }

    private fun writeToken(token: String) {
        Log.d("myLog", "write")
        sharedPreferences.write(token)
    }

    private fun validateAuth(login: String, password: String): Boolean {
        val pattern = Pattern.compile("^(.+)@(.+)$")
        if (pattern.matcher(login).matches() && password != "") {
            return true
        }
        view?.showMessage("Введите валидные данные")
        return false
    }

}