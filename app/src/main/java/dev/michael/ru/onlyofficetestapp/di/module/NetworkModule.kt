package dev.michael.ru.onlyofficetestapp.di.module

import dagger.Module
import dagger.Provides
import dev.michael.ru.onlyofficetestapp.network.OnlyOfficeImpl
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideOnlyOfficeImpl() = OnlyOfficeImpl()
}