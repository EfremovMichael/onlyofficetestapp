package dev.michael.ru.onlyofficetestapp.di


import dagger.Component
import dev.michael.ru.onlyofficetestapp.di.module.DocumentFragmentPresenterModule
import dev.michael.ru.onlyofficetestapp.di.module.LoginPresenterModule
import dev.michael.ru.onlyofficetestapp.di.module.MainPresenterModule
import dev.michael.ru.onlyofficetestapp.mvp.presenter.DocumentFragmentPresenterImpl
import dev.michael.ru.onlyofficetestapp.mvp.presenter.LoginPresenterImpl
import dev.michael.ru.onlyofficetestapp.mvp.presenter.MainPresenterImpl
import javax.inject.Singleton

@Singleton
@Component(modules = [MainPresenterModule::class, LoginPresenterModule::class, DocumentFragmentPresenterModule::class])
interface AppComponent {

    fun getMainPresenter(): MainPresenterImpl

    fun getLoginPresenter(): LoginPresenterImpl

    fun getDocumentFragmentPresenter(): DocumentFragmentPresenterImpl
}