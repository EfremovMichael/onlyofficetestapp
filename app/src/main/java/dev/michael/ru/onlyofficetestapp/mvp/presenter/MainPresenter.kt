package dev.michael.ru.onlyofficetestapp.mvp.presenter

import dev.michael.ru.onlyofficetestapp.model.User
import dev.michael.ru.onlyofficetestapp.mvp.view.MainView
import dev.michael.ru.onlyofficetestapp.network.OnlyOffice
import dev.michael.ru.onlyofficetestapp.storage.SharedPreferences
import io.reactivex.disposables.CompositeDisposable

interface MainPresenter : Presenter<MainView> {

    fun loadUserInformation()

}

class MainPresenterImpl(private val sharedPreferences: SharedPreferences,
                        private val onlyOffice: OnlyOffice) : MainPresenter {

    private var view: MainView? = null
    private val disposable = CompositeDisposable()

    override fun attachView(view: MainView) {
        this.view = view
    }

    override fun detachView() {
        disposable.clear()
        view = null
    }

    override fun loadUserInformation() {
        sharedPreferences.read()?.let {
            onlyOffice
                    .getMeInformation(it)
                    .subscribe { user, error ->
                        if (user != null) {
                            setUserInfo(user)
                        } else view?.showMessage(error.message)
                    }
        }
    }

    private fun setUserInfo(user: User) {
        user.response.apply {
            view?.setAvatar(avatarMedium)
            view?.setName(userName)
            view?.setEmail(email)
        }
    }
}