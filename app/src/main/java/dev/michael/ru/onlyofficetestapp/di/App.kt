package dev.michael.ru.onlyofficetestapp.di

import android.app.Application
import dev.michael.ru.onlyofficetestapp.di.module.ContextModule

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent.builder()
                .contextModule(ContextModule(applicationContext))
                .build()
    }

    companion object {

        private lateinit var component: AppComponent

        fun getComponent(): AppComponent {
            return component
        }
    }
}