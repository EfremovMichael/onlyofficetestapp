package dev.michael.ru.onlyofficetestapp.model.auth


data class UserAuthenticationInfo(
    val userName: String,
    val password: String
)