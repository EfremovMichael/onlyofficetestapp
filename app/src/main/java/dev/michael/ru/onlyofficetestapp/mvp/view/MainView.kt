package dev.michael.ru.onlyofficetestapp.mvp.view

interface MainView : View {

    fun setAvatar(url: String)
    fun setEmail(email: String)
    fun setName(name: String)
    fun showMessage(message: String?)

}