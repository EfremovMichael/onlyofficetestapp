package dev.michael.ru.onlyofficetestapp.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ContextModule(val context: Context) {

    @Singleton
    @Provides
    fun provideContext() = context.applicationContext!!

}