package dev.michael.ru.onlyofficetestapp.network

import dev.michael.ru.onlyofficetestapp.model.User
import dev.michael.ru.onlyofficetestapp.model.auth.Authentication
import dev.michael.ru.onlyofficetestapp.model.auth.UserAuthenticationInfo
import dev.michael.ru.onlyofficetestapp.model.document.Document
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface OnlyOfficeApi {

    @Headers("Accept: application/json")
    @POST("api/2.0/authentication")
    fun auth(@Body UserInfo: UserAuthenticationInfo): Single<Authentication>

    @GET("api/2.0/people/@self")
    fun getMeInformation(@Header("Authorization") token: String): Single<User>

    @GET("api/2.0/files/@my")
    fun getMyFiles(@Header("Authorization") token: String): Single<Document>

    @GET("api/2.0/files/@common")
    fun getCommonFiles(@Header("Authorization") token: String): Single<Document>

    @GET("api/2.0/files/{folderId}")
    fun getFolderById(@Header("Authorization") token: String,
                      @Path("folderId") folderId: Int): Single<Document>

    companion object Factory {

        private const val BASE_URL = "https://michaelefremov.onlyoffice.eu"

        fun create(): OnlyOfficeApi = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build().create(OnlyOfficeApi::class.java)
    }
}