package dev.michael.ru.onlyofficetestapp.mvp.presenter

import dev.michael.ru.onlyofficetestapp.model.document.MyFiles
import dev.michael.ru.onlyofficetestapp.mvp.view.DocumentFragmentView
import dev.michael.ru.onlyofficetestapp.network.OnlyOffice
import dev.michael.ru.onlyofficetestapp.network.OnlyOfficeImpl
import dev.michael.ru.onlyofficetestapp.storage.SharedPreferences

interface DocumentFragmentPresenter : Presenter<DocumentFragmentView> {

    fun loadMyFiles()
    fun loadFolder(folderId: Int)
    fun loadCommonFiles()
}

class DocumentFragmentPresenterImpl(
        private val onlyOffice: OnlyOffice,
        private val sharedPreferences: SharedPreferences) : DocumentFragmentPresenter {

    private var view: DocumentFragmentView? = null

    override fun attachView(view: DocumentFragmentView) {
        this.view = view
    }

    override fun detachView() {
        view = null
    }

    override fun loadMyFiles() {
        (sharedPreferences.read()?.let {
            onlyOffice.getFiles(it).subscribe { response, error ->
                if (response != null) {
                    view?.updateUi(MyFiles(response.folders, response.files))
                } else view?.showMessage(error.message)
            }
        })
    }

    override fun loadCommonFiles() {
        (sharedPreferences.read()?.let {
            onlyOffice.getCommonFiles(it).subscribe { response, error ->
                if (response != null) {
                    view?.updateUi(MyFiles(response.folders, response.files))
                } else view?.showMessage(error.message)
            }
        })
    }

    override fun loadFolder(folderId: Int) {
        (sharedPreferences.read()?.let {
            onlyOffice.getFolderById(it, folderId).subscribe { response, error ->
                if (response != null) {
                    view?.updateUi(MyFiles(response.folders, response.files))
                } else view?.showMessage(error.message)
            }
        })
    }
}