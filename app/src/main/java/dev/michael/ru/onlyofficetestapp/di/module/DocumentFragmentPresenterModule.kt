package dev.michael.ru.onlyofficetestapp.di.module

import dagger.Module
import dagger.Provides
import dev.michael.ru.onlyofficetestapp.mvp.presenter.DocumentFragmentPresenterImpl
import dev.michael.ru.onlyofficetestapp.network.OnlyOfficeImpl
import dev.michael.ru.onlyofficetestapp.storage.SharedPreferences
import javax.inject.Singleton

@Module(includes = [NetworkModule::class, StorageModule::class])
class DocumentFragmentPresenterModule {

    @Singleton
    @Provides
    fun provideDocumentsFragmentPresenter(preferences: SharedPreferences, onlyOfficeImpl: OnlyOfficeImpl) =
            DocumentFragmentPresenterImpl(onlyOfficeImpl, preferences)
}