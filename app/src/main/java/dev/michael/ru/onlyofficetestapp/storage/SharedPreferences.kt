package dev.michael.ru.onlyofficetestapp.storage

import android.content.Context
import android.content.SharedPreferences

private const val SHARED_PREFERENCE = "SHARED_PREFERENCE"
private const val USER_TOKEN = "USER_TOKEN"

class SharedPreferences(context: Context) {

    private val sharedPreferences: SharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCE, Context.MODE_PRIVATE)

    fun write(value: String) {
        sharedPreferences.edit().putString(USER_TOKEN, value).apply()
    }

    fun read(): String? = sharedPreferences.getString(USER_TOKEN, null)

    fun delete() = sharedPreferences.edit().clear().apply()
}