package dev.michael.ru.onlyofficetestapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso
import dev.michael.ru.onlyofficetestapp.di.App
import dev.michael.ru.onlyofficetestapp.mvp.presenter.MainPresenter
import dev.michael.ru.onlyofficetestapp.mvp.view.MainView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

//MainActivity корневая активити для фраментов
class MainActivity :
        AppCompatActivity(),
        MainView,
        NavigationView.OnNavigationItemSelectedListener {

    private lateinit var presenter: MainPresenter
    private lateinit var headView: View

    //Nav header view
    private lateinit var avatarImageView: ImageView
    private lateinit var userNameTv: TextView
    private lateinit var userEmailTv: TextView

    private var fragment: Fragment? = null
    private lateinit var fragmentManager: FragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        supportActionBar?.setTitle(R.string.nav_item_my_documents)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        //Init Nav header view
        headView = navView.getHeaderView(0)
        headView.apply {
            avatarImageView = findViewById(R.id.avatarImageView)
            userNameTv = findViewById(R.id.userNameTv)
            userEmailTv = findViewById(R.id.userEmailTv)
        }

        presenter = App.getComponent().getMainPresenter()
        presenter.attachView(this)
        presenter.loadUserInformation()

        fragmentManager = supportFragmentManager
        fragment = fragmentManager.findFragmentById(R.id.fragmentContainer)
        if (fragment == null) {
            fragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainer, DocumentsFragment.newInstance(0))
                    .commit()
            navView.setCheckedItem(R.id.navItemMyDocuments)
        }

    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    //Back to LoginActivity and delete token from storage
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_exit -> {
                val intent = Intent(this, LoginActivity::class.java)
                intent.putExtra(LoginActivity.DELETE_TOKEN, true)
                startActivity(intent)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    //Side menu navigation
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        fragmentManager = supportFragmentManager
        when (item.itemId) {
            R.id.navItemMyDocuments -> {
                toolbar.setTitle(R.string.nav_item_my_documents)
                fragmentManager.beginTransaction().replace(R.id.fragmentContainer, DocumentsFragment.newInstance(0)).commit()
            }
            R.id.navItemCommonDocuments -> {
                toolbar.setTitle(R.string.nav_item_common_documents)
                fragmentManager.beginTransaction().replace(R.id.fragmentContainer, DocumentsFragment.newInstance(1)).commit()
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun setAvatar(url: String) {
        Picasso.get().load(url).into(avatarImageView)
    }

    override fun setEmail(email: String) {
        userEmailTv.text = email
    }

    override fun setName(name: String) {
        userNameTv.text = name
    }

    override fun showMessage(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    companion object {

        @JvmStatic
        fun newInstance(context: Context): Intent = Intent(context, MainActivity::class.java)

    }
}
