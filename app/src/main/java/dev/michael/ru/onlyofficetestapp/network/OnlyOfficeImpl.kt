package dev.michael.ru.onlyofficetestapp.network

import dev.michael.ru.onlyofficetestapp.model.User
import dev.michael.ru.onlyofficetestapp.model.auth.Authentication
import dev.michael.ru.onlyofficetestapp.model.auth.UserAuthenticationInfo
import dev.michael.ru.onlyofficetestapp.model.document.Document
import dev.michael.ru.onlyofficetestapp.model.document.Response
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface OnlyOffice {

    fun getToken(userName: String, password: String): Single<Authentication>
    fun getMeInformation(token: String): Single<User>
    fun getFiles(token: String): Single<Response>
    fun getCommonFiles(token: String): Single<Response>
    fun getFolderById(token: String, id: Int): Single<Response>

}

class OnlyOfficeImpl : OnlyOffice {

    private val onlyOfficeApi = OnlyOfficeApi.create()

    override fun getToken(userName: String, password: String): Single<Authentication> =
            onlyOfficeApi.auth(UserAuthenticationInfo(userName, password))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())


    override fun getMeInformation(token: String): Single<User> =
            onlyOfficeApi.getMeInformation(token)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    override fun getFiles(token: String): Single<Response> =
            onlyOfficeApi.getMyFiles(token)
                    .map { document -> document.response }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    override fun getCommonFiles(token: String): Single<Response> =
            onlyOfficeApi.getCommonFiles(token)
                    .map { document -> document.response }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())

    override fun getFolderById(token: String, id: Int): Single<Response> =
            onlyOfficeApi.getFolderById(token, id)
                    .map { document -> document.response }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
}