package dev.michael.ru.onlyofficetestapp.model.auth


data class Authentication(
    val status: Int,
    val response: Response
)

data class Response(
    val token: String,
    val expires: String,
    val sms: Boolean,
    val phoneNoise: String
)