package dev.michael.ru.onlyofficetestapp.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import dev.michael.ru.onlyofficetestapp.storage.SharedPreferences
import javax.inject.Singleton

@Module(includes = [ContextModule::class])
class StorageModule {

    @Singleton
    @Provides
    fun provideSharedPreferences(context: Context) = SharedPreferences(context)
}