package dev.michael.ru.onlyofficetestapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ProgressBar
import android.widget.Toast
import dev.michael.ru.onlyofficetestapp.di.App
import dev.michael.ru.onlyofficetestapp.mvp.presenter.LoginPresenter
import dev.michael.ru.onlyofficetestapp.mvp.view.LoginView
import kotlinx.android.synthetic.main.activity_login.*

private const val LOGIN_STATE = "LOGIN_STATE"
private const val PASSWORD_STATE = "PASSWORD_STATE"

class LoginActivity : AppCompatActivity(), LoginView {

    companion object {
        const val DELETE_TOKEN = "DELETE_TOKEN"
    }

    private var presenter: LoginPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        savedInstanceState?.apply {
            loginEditText.setText(getString(LOGIN_STATE))
            passwordEditText.setText(getString(PASSWORD_STATE))
        }
        presenter = App.getComponent().getLoginPresenter()
        presenter?.attachView(this)

        //Delete token  from storage
        if (intent.getBooleanExtra(DELETE_TOKEN, false)) {
            presenter?.deleteToken()
        }

        presenter?.checkToken()

        loginButton.setOnClickListener {
            val login = loginEditText.text.toString()
            val password = passwordEditText.text.toString()
            presenter?.getToken(login, password)
        }
    }

    //Save login and password
    override fun onSaveInstanceState(outState: Bundle?) {
        if (loginEditText.text.toString() != "" ||
                passwordEditText.text.toString() != "") {
            outState?.putString(LOGIN_STATE, loginEditText.text.toString())
            outState?.putString(PASSWORD_STATE, passwordEditText.text.toString())
        }
        super.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        presenter?.detachView()
        super.onDestroy()
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showProgressBar() {
        loginProgressBar.visibility = ProgressBar.VISIBLE
        loginProgressBar.isEnabled = false
    }

    override fun hideProgressBar() {
        loginProgressBar.visibility = ProgressBar.INVISIBLE
        loginProgressBar.isEnabled = true
    }

    override fun goNextActivity() {
        startActivity(MainActivity.newInstance(this))
        finish()
    }


}
