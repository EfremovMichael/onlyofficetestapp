package dev.michael.ru.onlyofficetestapp.model.document


enum class DocType(val type: Int){
    WORD(7),
    EXEL(5),
    POWERPOINT(6)
}

data class Document(
        val count: Int,
        val status: Int,
        val statusCode: Int,
        val response: Response
)

data class Response(
        val files: List<File>,
        val folders: List<Folder>,
        val current: Current,
        val pathParts: List<Int>,
        val startIndex: Int,
        val count: Int,
        val total: Int
)

data class Folder(
        val parentId: Int,
        val filesCount: Int,
        val foldersCount: Int,
        val id: Int,
        val title: String,
        val access: Int,
        val shared: Boolean,
        val rootFolderType: Int,
        val updatedBy: UpdatedBy,
        val created: String,
        val createdBy: CreatedBy,
        val updated: String,
        val providerItem: Boolean
)

data class UpdatedBy(
        val id: String,
        val displayName: String,
        val avatarSmall: String,
        val profileUrl: String
)

data class CreatedBy(
        val id: String,
        val displayName: String,
        val avatarSmall: String,
        val profileUrl: String
)

data class File(
        val folderId: Int,
        val version: Int,
        val versionGroup: Int,
        val contentLength: String,
        val pureContentLength: Int,
        val fileStatus: Int,
        val viewUrl: String,
        val webUrl: String,
        val fileType: Int,
        val fileExst: String,
        val comment: String,
        val id: Int,
        val title: String,
        val access: Int,
        val shared: Boolean,
        val rootFolderType: Int,
        val updatedBy: UpdatedBy,
        val created: String,
        val createdBy: CreatedBy,
        val updated: String,
        val providerItem: Boolean
)

data class Current(
        val parentId: Int,
        val filesCount: Int,
        val foldersCount: Int,
        val isShareable: Boolean,
        val id: Int,
        val title: String,
        val access: Int,
        val shared: Boolean,
        val rootFolderType: Int,
        val updatedBy: UpdatedBy,
        val created: String,
        val createdBy: CreatedBy,
        val updated: String,
        val providerItem: Boolean
)

data class MyFiles(
        val folders: List<Folder>,
        val files: List<File>
)