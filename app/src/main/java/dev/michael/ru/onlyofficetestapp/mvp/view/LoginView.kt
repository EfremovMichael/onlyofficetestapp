package dev.michael.ru.onlyofficetestapp.mvp.view

interface LoginView : View {

    fun showMessage(message: String)

    fun showProgressBar()

    fun hideProgressBar()

    fun goNextActivity()

}